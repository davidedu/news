module.exports = {
  printWidth: 100,
  useTabs: false,
  bracketSpacing: true,
  arrowParens: 'always',
  singleQuote: true,
  proseWrap: 'always',
  tabWidth: 2,
  endOfLine: 'auto',
};
