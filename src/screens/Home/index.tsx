import { unwrapResult } from '@reduxjs/toolkit';
import { useEffect, useRef, useState } from 'react';
import { FlatList, Text, View } from 'react-native';
import Toast from 'react-native-toast-message';
import NewsList from '../../components/NewsList';
import SearchInput from '../../components/SearchInput';
import { ECategories } from '../../enums/ECategories';
import useAppDispatch from '../../hooks/useAppDispatch';
import useAppSelector from '../../hooks/useAppSelector';
import { getNews } from '../../redux/actions/news.action';
import { setLimitOfPagination, setSearchFilter } from '../../redux/reducers/news.reducer';
import styles from './styles';
import CategoriesList from '../../components/CategoriesList';

var firstOpen = true;

export default function Home(): JSX.Element {
  const { categoryReducer, newsReducer } = useAppSelector((store) => store);
  const dispatch = useAppDispatch();
  const [page, setPage] = useState<number>(0);
  const [loading, setLoading] = useState<boolean>(false);
  const [loadingRefresh, setLoadingRefresh] = useState<boolean>(false);
  const listRef = useRef<FlatList>(null);
  const PAGE_SIZE = 10;

  useEffect(() => {
    getPaginatedNews();
  }, []);

  useEffect(() => {
    // No ejecuta el useEffect la primera vez
    if (firstOpen) {
      firstOpen = false;
      return;
    }

    getPaginatedNews(true);
  }, [categoryReducer.categorySelected]);

  /**
   * @description Obtiene el listado de noticias
   * @param {boolean} isRefresh Verifica si esta refrescando la informacion
   */
  const getPaginatedNews = async (isRefresh = false) => {
    try {
      if (loading || loadingRefresh || (!isRefresh && newsReducer.limitOfPagination)) {
        return;
      }

      const newPage = isRefresh ? 1 : page + 1;

      setLoading(true);

      await dispatch(
        getNews({
          page: newPage,
          country: 'us',
          pageSize: PAGE_SIZE,
          category:
            categoryReducer.categorySelected === ECategories.All
              ? undefined
              : categoryReducer.categorySelected,
          q: newsReducer.searchFilter || undefined,
        })
      ).then(unwrapResult);

      setPage(newPage);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      const messagesError = {
        rateLimited: 'Se superó el límite de peticiones por día.',
        default: 'Ocurrio un error inesperado',
      };
      Toast.show({
        type: 'error',
        text1: 'Error',
        text2: messagesError[error?.response?.data?.code] || messagesError.default,
      });
    }
  };

  /**
   * @description Refresca la informacion del listado de noticias
   */
  const onRefresh = async () => {
    setLoadingRefresh(true);
    dispatch(setLimitOfPagination(false));
    await getPaginatedNews(true);
    setLoadingRefresh(false);
  };

  return (
    <>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Noticias</Text>
        <Text style={styles.subTitle}>Nuevas noticias del mundo</Text>
      </View>

      <SearchInput
        value={newsReducer.searchFilter}
        onChangeText={(text) => dispatch(setSearchFilter(text))}
        onSearch={onRefresh}
      />

      <CategoriesList />

      <NewsList
        listRef={listRef}
        news={newsReducer.news}
        showLoadingMore={loading && !loadingRefresh}
        showLoadingRefresh={loadingRefresh}
        onRefresh={onRefresh}
        onLoadMore={() => getPaginatedNews()}
      />
    </>
  );
}
