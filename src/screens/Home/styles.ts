import { StyleSheet } from 'react-native';
import themes from '../../styles/themes';
import { HORIZONTAL_SPACE } from '../../constants/styles';
import isIos from '../../utils/isIos';

export default StyleSheet.create({
  titleContainer: {
    backgroundColor: themes().white,
    paddingHorizontal: HORIZONTAL_SPACE,
    paddingVertical: 20,
    paddingTop: isIos ? 40 : 20,
  },
  title: {
    color: themes().text,
    fontSize: 40,
    fontWeight: '700',
  },
  subTitle: {
    color: themes(0.6).text,
    fontSize: 15,
  },
});
