import { useRoute } from '@react-navigation/native';
import { Image, ScrollView, Text, View } from 'react-native';
import Header from '../../components/Header';
import INew from '../../interfaces/redux/news/INew';
import styles from './styles';

export default function Article(): JSX.Element {
  const route = useRoute();
  const article = route.params as INew;

  return (
    <>
      <Header showSearch={false} transparent />

      <View style={styles.imageContainer}>
        {article.urlToImage ? (
          <Image source={{ uri: article.urlToImage }} style={styles.image} />
        ) : (
          <View style={styles.noImage}></View>
        )}

        <View style={styles.date}>
          <Text style={styles.textWhite}>
            {new Date(article.publishedAt).toLocaleDateString('es-VE')}
          </Text>
        </View>
        {article.author && (
          <View style={styles.author}>
            <Text>By: {article.author}</Text>
          </View>
        )}
      </View>

      <ScrollView style={styles.container}>
        <Text style={styles.source}>{article.source.name}</Text>
        <Text style={styles.desc}>{article.description}</Text>
        <Text style={styles.desc}>{article.content}</Text>
      </ScrollView>
    </>
  );
}
