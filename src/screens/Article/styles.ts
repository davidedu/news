import { StyleSheet } from 'react-native';
import themes from '../../styles/themes';
import { HORIZONTAL_SPACE } from '../../constants/styles';

export default StyleSheet.create({
  imageContainer: {
    position: 'relative',
  },
  date: {
    backgroundColor: themes(0.8).primary,
    position: 'absolute',
    bottom: 10,
    left: 10,
    color: themes().white,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 100,
  },
  author: {
    backgroundColor: themes(0.8).gray,
    position: 'absolute',
    right: 10,
    bottom: 10,
    color: themes().text,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 100,
  },
  container: {
    backgroundColor: themes().white,
    paddingHorizontal: HORIZONTAL_SPACE,
    paddingTop: 15,
    borderTopEndRadius: 50,
    borderTopStartRadius: 50,
  },
  image: {
    width: '100%',
    height: 330,
  },
  noImage: {
    width: '100%',
    height: 330,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
  },
  source: {
    fontSize: 25,
    color: themes().text,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  desc: {
    fontSize: 15,
    color: themes().text,
    marginBottom: 10,
  },
  textWhite: {
    color: themes().white,
  },
});
