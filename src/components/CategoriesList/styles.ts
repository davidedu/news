import { StyleSheet } from 'react-native';
import themes from '../../styles/themes';

export default StyleSheet.create({
  container: {
    backgroundColor: themes().white,
    minHeight: 40,
    maxHeight: 40,
  },
  item: {
    backgroundColor: themes(0.5).gray,
    borderRadius: 100,
    padding: 5,
    paddingHorizontal: 10,
    height: 30,
  },
  itemSelected: {
    backgroundColor: themes().primary,
  },
  textWhite: {
    color: themes().white,
  },
});
