import { FlatList, Text, TouchableOpacity } from 'react-native';
import { HORIZONTAL_SPACE } from '../../constants/styles';
import useAppDispatch from '../../hooks/useAppDispatch';
import useAppSelector from '../../hooks/useAppSelector';
import { changeCategory } from '../../redux/reducers/categories.reducer';
import styles from './styles';

export default function CategoriesList(): JSX.Element {
  const { categoryReducer } = useAppSelector((store) => store);
  const dispatch = useAppDispatch();
  const CATEGORY_LEFT_SPACE = 7;

  return (
    <FlatList
      style={styles.container}
      data={categoryReducer.categories}
      renderItem={({ item, index }) => (
        <TouchableOpacity
          // eslint-disable-next-line react-native/no-inline-styles
          style={{
            ...styles.item,
            marginLeft: index === 0 ? HORIZONTAL_SPACE : CATEGORY_LEFT_SPACE,
            marginRight: index === categoryReducer.categories.length - 1 ? CATEGORY_LEFT_SPACE : 0,
            ...(categoryReducer.categorySelected === item.value ? styles.itemSelected : {}),
          }}
          onPress={() => dispatch(changeCategory(item.value))}
        >
          <Text style={categoryReducer.categorySelected === item.value ? styles.textWhite : {}}>
            {item.title}
          </Text>
        </TouchableOpacity>
      )}
      keyExtractor={(item) => item.value}
      horizontal
    />
  );
}
