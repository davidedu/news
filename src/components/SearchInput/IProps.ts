export default interface IProps {
  value: string;
  onChangeText: (value: string) => void;
  onSearch: () => void;
}
