import { TextInput, TouchableOpacity, View } from 'react-native';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import IProps from './IProps';
import styles from './styles';
import themes from '../../styles/themes';

export default function SearchInput({ value, onChangeText, onSearch }: IProps): JSX.Element {
  return (
    <View style={styles.container}>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Buscar..."
          placeholderTextColor={themes(0.3).text}
          keyboardType="web-search"
          returnKeyType="search"
          value={value}
          onChangeText={onChangeText}
          onSubmitEditing={onSearch}
          cursorColor={themes().text}
        />
        <TouchableOpacity onPress={onSearch}>
          <EvilIcons name="search" size={30} color={themes().text} />
        </TouchableOpacity>
      </View>
    </View>
  );
}
