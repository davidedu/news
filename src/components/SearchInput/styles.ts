import { StyleSheet } from 'react-native';
import themes from '../../styles/themes';
import { HORIZONTAL_SPACE } from '../../constants/styles';
import isIos from '../../utils/isIos';

export default StyleSheet.create({
  container: {
    backgroundColor: themes().white,
    paddingHorizontal: HORIZONTAL_SPACE,
    paddingVertical: 20,
  },
  inputContainer: {
    backgroundColor: themes(0.3).gray,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 100,
    paddingHorizontal: 15,
    paddingVertical: isIos ? 10 : 0,
  },
  input: {
    flex: 1,
    color: themes().text,
  },
});
