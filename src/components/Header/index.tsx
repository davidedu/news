import { TouchableOpacity, View } from 'react-native';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import IProps from './IProps';
import styles from './styles';
import { ParamListBase, useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

export default function Header({
  showBack = true,
  showSearch = true,
  transparent = false,
}: IProps): JSX.Element {
  const navigation = useNavigation<StackNavigationProp<ParamListBase>>();

  return (
    <View style={[styles.container, transparent && styles.transparent]}>
      <View style={styles.left}>
        {showBack && (
          <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
            <Icon name="arrow-left" size={15} />
          </TouchableOpacity>
        )}
      </View>
      <View style={styles.right}>
        {showSearch && (
          <TouchableOpacity style={styles.back}>
            <EvilIcons name="search" size={20} />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
}
