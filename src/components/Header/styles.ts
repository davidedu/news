import { StyleSheet } from 'react-native';
import themes from '../../styles/themes';
import { HORIZONTAL_SPACE } from '../../constants/styles';
import isIos from '../../utils/isIos';

export default StyleSheet.create({
  transparent: {
    position: 'absolute',
    zIndex: 10000,
    backgroundColor: 'transparent',
  },
  container: {
    backgroundColor: themes().white,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: HORIZONTAL_SPACE,
    paddingVertical: 20,
    paddingTop: isIos ? 30 : 20
  },
  left: {},
  back: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: themes(0.8).gray,
    borderRadius: 100,
    width: 35,
    height: 35,
  },
  right: {},
});
