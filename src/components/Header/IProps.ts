export default interface IProps {
  showBack?: boolean;
  showSearch?: boolean;
  transparent?: boolean;
}
