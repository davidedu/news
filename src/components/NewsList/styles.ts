import { StyleSheet } from 'react-native';
import themes from '../../styles/themes';
import { HORIZONTAL_SPACE } from '../../constants/styles';

export default StyleSheet.create({
  container: {
    backgroundColor: themes().white,
    paddingHorizontal: HORIZONTAL_SPACE,
  },

  indicator: {
    marginBottom: 15,
  },

  empty: {
    alignItems: 'center',
  },
  emptyText: {
    color: themes().text,
    fontSize: 15,
  },
});
