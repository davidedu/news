import { ActivityIndicator, FlatList, RefreshControl, View, Text } from 'react-native';
import themes from '../../styles/themes';
import ArticleCard from '../ArticleCard';
import IProps from './IProps';
import styles from './styles';

export default function NewsList({
  listRef,
  news,
  showLoadingMore,
  showLoadingRefresh,
  onRefresh,
  onLoadMore,
}: IProps): JSX.Element {
  return (
    <FlatList
      ref={listRef}
      removeClippedSubviews
      updateCellsBatchingPeriod={10}
      style={styles.container}
      data={news}
      renderItem={({ item }) => <ArticleCard article={item} />}
      keyExtractor={(item, index) => `${index}-${item.source.id}`}
      refreshControl={
        <RefreshControl
          refreshing={showLoadingRefresh}
          tintColor={themes().primary}
          onRefresh={onRefresh}
        />
      }
      ListEmptyComponent={() =>
        !showLoadingMore &&
        news.length === 0 && (
          <View style={styles.empty}>
            <Text style={styles.emptyText}>No se encontraron noticias</Text>
          </View>
        )
      }
      onEndReachedThreshold={0.6}
      onEndReached={() => {
        if (news.length > 0) {
          onLoadMore();
        }
      }}
      ListFooterComponent={() =>
        showLoadingMore && <ActivityIndicator style={styles.indicator} color={themes().primary} />
      }
    />
  );
}
