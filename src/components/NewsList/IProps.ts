import { FlatList } from 'react-native';
import INew from '../../interfaces/redux/news/INew';

export default interface IProps {
  listRef?: React.MutableRefObject<FlatList<any>>;
  news: INew[];
  showLoadingMore?: boolean;
  showLoadingRefresh?: boolean;
  onRefresh?: () => void;
  onLoadMore?: () => void;
}
