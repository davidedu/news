import INew from '../../interfaces/redux/news/INew';

export default interface IProps {
  article: INew;
}
