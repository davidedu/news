import { StyleSheet } from 'react-native';
import themes from '../../styles/themes';

export default StyleSheet.create({
  container: {
    backgroundColor: themes().white,
    marginBottom: 15,
    flexDirection: 'row',
  },
  image: {
    width: 110,
    height: 110,
    borderRadius: 15,
    flex: 0,
  },
  noImage: {
    width: 110,
    height: 110,
    borderColor: themes().gray,
    borderWidth: 1,
    borderRadius: 15,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
  },

  info: {
    flex: 1,
    paddingLeft: 10,
    justifyContent: 'space-between',
  },
  date: {
    color: themes(0.5).text,
    fontSize: 10,
    marginBottom: 2,
  },
  title: {
    color: themes().text,
    fontSize: 15,
    fontWeight: 'bold',
    marginBottom: 2,
  },
  desc: {
    color: themes().text,
    fontSize: 12,
    marginBottom: 2,
  },

  source: {
    color: themes(0.5).text,
    fontSize: 10,
  },
});
