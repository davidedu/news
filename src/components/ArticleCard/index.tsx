import { Image, Text, TouchableOpacity, View } from 'react-native';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import IProps from './IProps';
import styles from './styles';
import { ParamListBase, useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { memo } from 'react';

const ArticleCard = memo(({ article }: IProps) => {
  const navigation = useNavigation<StackNavigationProp<ParamListBase>>();

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => navigation.navigate('Article', article)}
    >
      {article.urlToImage ? (
        <Image source={{ uri: article.urlToImage }} style={styles.image} />
      ) : (
        <View style={styles.noImage}>
          <EvilIcons name="image" size={20} />
        </View>
      )}

      <View style={styles.info}>
        <View>
          <Text style={styles.date}>
            {new Date(article.publishedAt).toLocaleDateString('es-VE')}
          </Text>
          <Text numberOfLines={2} style={styles.title}>
            {article.title}
          </Text>
          <Text numberOfLines={3} style={styles.desc}>
            {article.description}
          </Text>
        </View>

        <Text style={styles.source}>Fuente: {article.source.name}</Text>
      </View>
    </TouchableOpacity>
  );
});

export default ArticleCard;
