/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import Routes from './Routes';
import { ReduxProvider, store } from './redux';
import Toast from 'react-native-toast-message';

function App(): JSX.Element {
  return (
    <ReduxProvider store={store}>
      <SafeAreaProvider>
        <Routes />
        <Toast />
      </SafeAreaProvider>
    </ReduxProvider>
  );
}

export default App;
