import axios from 'axios';
import { BASE_URL, API_KEY } from '@env';

const axiosInstance = axios.create({
  baseURL: BASE_URL,
});

axiosInstance.interceptors.request.use(async (request) => {
  try {
    request.headers['X-Api-Key'] = API_KEY;

    return Promise.resolve(request);
  } catch (error) {
    return Promise.reject(error);
  }
});

export default axiosInstance;
