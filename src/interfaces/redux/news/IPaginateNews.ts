import INew from './INew';

export default interface IPaginateNews {
  status: string;
  totalResults: number;
  articles: INew[];
}
