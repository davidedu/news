export default interface IParamsGetNewsList {
  country?: string;
  category?: string;
  q?: string;
  pageSize?: number;
  page: number;
}
