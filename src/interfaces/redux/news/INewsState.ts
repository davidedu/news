import INew from './INew';

export interface INewsState {
  news: INew[];
  searchFilter: string;
  limitOfPagination: boolean;
}
