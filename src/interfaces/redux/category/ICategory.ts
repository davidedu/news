import { ECategories } from '../../../enums/ECategories';

export interface ICategory {
  title: string;
  value: ECategories;
}
