import { ECategories } from '../../../enums/ECategories';
import { ICategory } from './ICategory';

export default interface ICategoriesState {
  categories: ICategory[];
  categorySelected: ECategories;
}
