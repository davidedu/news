import { NativeStackNavigationOptions } from '@react-navigation/native-stack';
import { FC } from 'react';

export default interface IRoute {
  name: string;
  component: FC<any>;
  options?: NativeStackNavigationOptions;
}
