import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import newsSlice from './reducers/news.reducer';
import categorySlice from './reducers/categories.reducer';

const store = configureStore({
  reducer: {
    newsReducer: newsSlice.reducer,
    categoryReducer: categorySlice.reducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }).concat(),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

export { Provider as ReduxProvider, store };
