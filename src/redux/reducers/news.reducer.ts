import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { INewsState } from '../../interfaces/redux/news/INewsState';
import { getNews } from '../actions/news.action';

const initialState: INewsState = {
  news: [],

  searchFilter: null,
  limitOfPagination: false,
};

const newsSlice = createSlice({
  initialState,
  name: 'newsReducer',
  reducers: {
    /**
     * @description Cambia el valor del filtro de busqueda
     * @param {WritableDraft<ICategoriesState>} state
     * @param {PayloadAction<ECategories>} action
     */
    setSearchFilter: (state, action: PayloadAction<string>) => {
      state.searchFilter = action.payload;
    },

    /**
     * @description Cambia el valor del filtro de busqueda
     * @param {WritableDraft<ICategoriesState>} state
     * @param {PayloadAction<ECategories>} action
     */
    setLimitOfPagination: (state, action: PayloadAction<boolean>) => {
      state.limitOfPagination = action.payload;
    },
  },
  extraReducers: (builder) => {
    // Obtiene el listado de los articulos
    builder.addCase(getNews.fulfilled, (state, action) => {
      // Si es la primera paginacion no agrega mas info con el estado anterior
      if (action.meta.arg.page === 1) {
        state.news = action.payload.articles;
        return;
      }

      if (action.payload.articles.length < action.meta.arg.pageSize) {
        state.limitOfPagination = true;
      }

      state.news = [...state.news, ...action.payload.articles];
    });
  },
});

export const { setSearchFilter, setLimitOfPagination } = newsSlice.actions;

export default newsSlice;
