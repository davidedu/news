import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { ECategories } from '../../enums/ECategories';
import ICategoriesState from '../../interfaces/redux/category/ICategoriesState';

const initialState: ICategoriesState = {
  categories: [
    {
      title: 'Todos',
      value: ECategories.All,
    },
    {
      title: 'Empresas',
      value: ECategories.Business,
    },
    {
      title: 'Entretenimiento',
      value: ECategories.Entertainment,
    },
    {
      title: 'General',
      value: ECategories.General,
    },
    {
      title: 'Salud',
      value: ECategories.Health,
    },
    {
      title: 'Ciencia',
      value: ECategories.Science,
    },
    {
      title: 'Deportes',
      value: ECategories.Sports,
    },
    {
      title: 'Tecnología',
      value: ECategories.Technology,
    },
  ],
  categorySelected: ECategories.All,
};

const categorySlice = createSlice({
  initialState,
  name: 'categoryReducer',
  reducers: {
    /**
     * @description Cambia la categoria seleccionada
     * @param {WritableDraft<ICategoriesState>} state
     * @param {PayloadAction<ECategories>} action
     */
    changeCategory: (state, action: PayloadAction<ECategories>) => {
      state.categorySelected = action.payload;
    },
  },
});

export const { changeCategory } = categorySlice.actions;

export default categorySlice;
