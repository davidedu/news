import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosInstance from '../../utils/axiosInstance';
import IParamsGetNewsList from '../../interfaces/redux/news/IParamsGetNewsList';
import IPaginateNews from '../../interfaces/redux/news/IPaginateNews';

/**
 * @description Obtiene el listado de las noticias paginado
 * @returns {AsyncThunk<IPaginateNews, IParamsGetNewsList, AsyncThunkConfig>}
 */
export const getNews = createAsyncThunk(
  'new/list',
  async (
    { page = 1, category, country, pageSize = 10, q }: IParamsGetNewsList,
    { rejectWithValue }
  ) => {
    try {
      const { data: newsResponse } = await axiosInstance.get<IPaginateNews>('/top-headlines', {
        params: {
          page,
          category: category || undefined,
          country: country || undefined,
          pageSize,
          q: q ? q.trim() : undefined,
        },
      });
      return newsResponse;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);
