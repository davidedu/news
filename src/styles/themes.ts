const themes = (opacity: number = 1) => ({
  primary: `rgba(42, 117, 187, ${opacity})`,
  secondary: `rgba(255, 203, 5, ${opacity})`,
  blueDark: `rgba(0, 60, 116, ${opacity})`,
  white: `rgba(254, 254, 254, ${opacity})`,
  gray: `rgba(210, 211, 213, ${opacity})`,
  background: `rgba(249, 249, 249, ${opacity})`,
  text: `rgba(40, 40, 40, ${opacity})`,
  danger: `rgba(187, 33, 36, ${opacity})`,
  success: `rgba(34, 187, 51, ${opacity})`,
  warning: `rgba(255, 204, 0, ${opacity})`,
});

export default themes;
