import IRoute from '../interfaces/routes/IRoutes';
import Article from '../screens/Article';
import Home from '../screens/Home';
import themes from '../styles/themes';
import isIos from '../utils/isIos';

export const routes: IRoute[] = [
  {
    name: 'Home',
    component: Home,
    options: {
      headerShown: false,
      statusBarColor: themes().white,
      statusBarStyle: isIos ? undefined : 'dark',
      headerTransparent: true,
    },
  },
  {
    name: 'Article',
    component: Article,
    options: {
      headerShown: false,
      statusBarColor: themes().white,
      statusBarStyle: isIos ? undefined : 'dark',
      headerTransparent: true,
      animation: 'slide_from_right',
    },
  },
];
